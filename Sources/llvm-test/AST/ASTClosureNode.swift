//
//  ASTClosureNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ASTClosureNode : ASTCommandNode {
    
    var commands : [ASTCommandNode] = [ASTCommandNode]()
    
    func addCommand(_ command : ASTCommandNode) {
        
        commands.append(command)
        self.type = command.type
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        
        try traversable.createClosure(self)
        
    }
    
}
