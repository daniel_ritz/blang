//
//  ASTNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

protocol ASTNode {
    
    func accept(_ traversable : ASTTraversable) throws
    
}
