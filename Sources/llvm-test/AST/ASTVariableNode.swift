//
//  ASTVariableNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ASTValueNode : ASTCommandNode {
    
}

class ASTVariableNode : ASTValueNode {
    
    static var counter : Int = 0
    
    let name: String
    
    convenience override init() {
        
        self.init(type: .Int)
        
    }
    
    convenience init(type : ASTValueType) {
        
        ASTVariableNode.counter += 1
        self.init("glob" + ASTVariableNode.counter.description, type: type)
        
    }
    
    convenience init(_ name: String) {
        
        self.init(name, type: .Int)
        
    }
    
    init(_ name: String, type: ASTValueType) {
        
        self.name = name
        super.init()
        self.type = type
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createGlobal(self)
    }
    
}

