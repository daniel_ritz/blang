//
//  ASTTypeNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

enum ASTValueType {
    case Int
    case Float
    case String
    case Bool
}
