//
//  ASTConstantNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ASTConstant : ASTValueNode {
    
    init(type: ASTValueType) {
        
        super.init()
        self.type = type
        
    }
    
    static func forValue(_ value : String) throws -> ASTConstant {
        
        if value.matches("[0-9]+") {
            return ASTIntConstant(value: Int(value)!)
        } else if value.matches("[0-9]+\\.[0-9]+") {
            return ASTFloatConstant(value: Double(value)!)
        }
        
        throw ParserError.LexicalError(message: "Const: unknown type: " + value)
        
    }
    
}

class ASTIntConstant : ASTConstant {
    
    let value : Int
    
    init(value : Int) {
        
        self.value = value
        super.init(type: .Int)
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createIntConstant(self)
    }
    
}

class ASTFloatConstant : ASTConstant {
    
    let value : Double
    
    init(value : Double) {
        
        self.value = value
        super.init(type: .Float)
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createFloatConstant(self)
    }
    
}

class ASTStringConstant : ASTConstant {
    
    let value : String
    
    init(value : String) {
        
        self.value = value
        super.init(type: .String)
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createStringConstant(self)
    }
    
}

class ASTBoolConstant : ASTConstant {
    
    let value : Bool
    
    init(value : Bool) {
        
        self.value = value
        super.init(type: .Bool)
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createBoolConstant(self)
    }
    
}
