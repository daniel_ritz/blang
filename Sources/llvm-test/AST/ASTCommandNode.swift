//
//  ASTCommandNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ASTCommandNode : ASTNode {
    
    var type : ASTValueType = .Int
    
    func accept(_ traversable: ASTTraversable) throws {
        
    }
    
}

class ASTAssignmentNode : ASTCommandNode {
    
    let variable : ASTVariableNode
    let value : ASTCommandNode
    
    init(variable: ASTVariableNode, value: ASTCommandNode) {
        
        self.variable = variable
        self.value = value
        super.init()
        self.type = value.type
        self.variable.type = value.type
        
    }
    
    func updateType(){
        self.variable.type = self.value.type
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        
        try traversable.createAssignment(self)
    }
    
}

class ASTMultiAssignmentNode : ASTCommandNode {
    
    var assignments : [ASTAssignmentNode] = [ASTAssignmentNode]()
    
    init(variables: [ASTVariableNode], values: [ASTCommandNode]) {
        
        for i in 0...(variables.count - 1) {
            
            assignments.append(ASTAssignmentNode(variable: variables[i], value: values[i]))
            
        }
        
    }
    
    
    
    func addAssigment(_ assigment : ASTAssignmentNode) {
        
        assignments.append(assigment)
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createMultiAssignment(self)
    }
    
}

class ASTArithmeticNode : ASTCommandNode {
    
    enum Operation {
        case add
        case sub
        case mul
        case div
        static func forSymbol(_ symbol : String) throws -> Operation{
            switch symbol {
            case "+":
                return Operation.add
            case "-":
                return Operation.sub
            case "*":
                return Operation.mul
            case "/":
                return Operation.div
            default:
                throw ParserError.LexicalError(message: "Unknown operation")
            }
        }
    }
    
    let lhs : ASTCommandNode
    let rhs : ASTCommandNode
    let operation : Operation
    
    init(lhs: ASTCommandNode, rhs: ASTCommandNode, operation: Operation) {
        
        self.lhs = lhs
        self.rhs = rhs
        self.operation = operation
        super.init()
        self.type = lhs.type
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        
        try traversable.createArithmeticOperation(self)
    }
    
}

class ASTCallNode : ASTCommandNode {
    
    let function : ASTFunctionNode
    let arguments : [ASTValueNode]
    
    override var type: ASTValueType {
        get {
            return function.type
        }
        set {
            return
        }
    }
    
    init(function: ASTFunctionNode, arguments: [ASTValueNode]) {
        
        self.function = function
        self.arguments = arguments
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createCall(self)
    }
    
}

class ASTComparisonNode : ASTCommandNode {
    
    enum Comparison {
        case lessThan
        case lessOrEquals
        case equals
        case notEquals
        case moreOrEquals
        case moreThan
        
        static func forSymbol(_ symbol : String) throws -> Comparison {
            switch symbol {
            case "<":
                return Comparison.lessThan
            case ">":
                return Comparison.moreThan
            case "==":
                return Comparison.equals
            case "!=":
                return Comparison.notEquals
            case "<=":
                return Comparison.lessOrEquals
            case ">=":
                return Comparison.moreOrEquals
            default:
                throw ParserError.LexicalError(message: "Unknown comparison")
            }
        }
    }
    
    let lhs : ASTCommandNode
    let rhs : ASTCommandNode
    let comparison : Comparison
    
    init(lhs: ASTCommandNode, rhs: ASTCommandNode, comparison: Comparison) {
        
        self.lhs = lhs
        self.rhs = rhs
        self.comparison = comparison
        super.init()
        self.type = .Bool
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createComparison(self)
    }
    
}

class ASTConditionNode : ASTCommandNode {
    
    let condition : ASTComparisonNode
    let trueClosure : ASTClosureNode = ASTClosureNode()
    let falseClosure : ASTClosureNode = ASTClosureNode()
    
    init(condition: ASTComparisonNode) {
        
        self.condition = condition
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createCondition(self)
    }
    
}

class ASTLoopNode : ASTCommandNode {
    
    let condition : ASTComparisonNode
    let body : ASTClosureNode = ASTClosureNode()
    
    init(condition: ASTComparisonNode) {
        
        self.condition = condition
        
    }
    
    override func accept(_ traversable: ASTTraversable) throws {
        try traversable.createLoop(self)
    }
    
}


