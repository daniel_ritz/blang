//
//  ASTTraversable.swift
//  llvm-test
//
//  Created by Daniel Ritz on 15.07.19.
//

protocol ASTTraversable {
    
    func createModule(_ module : ASTModule) throws
    func createFunction(_ function : ASTFunctionNode) throws
    func createGlobal(_ global : ASTVariableNode) throws
    func createIntConstant(_ constant : ASTIntConstant) throws
    func createFloatConstant(_ constant : ASTFloatConstant) throws
    func createStringConstant(_ constant : ASTStringConstant) throws
    func createBoolConstant(_ constant : ASTBoolConstant) throws
    func createArithmeticOperation(_ op : ASTArithmeticNode) throws
    func createCall(_ function : ASTCallNode) throws
    func createAssignment(_ assignment : ASTAssignmentNode) throws
    func createMultiAssignment(_ assignment : ASTMultiAssignmentNode) throws
    func createComparison(_ comparison : ASTComparisonNode) throws
    func createCondition(_ condition : ASTConditionNode) throws
    func createLoop(_ loop : ASTLoopNode) throws
    func createClosure(_ closure : ASTClosureNode) throws
    
}
