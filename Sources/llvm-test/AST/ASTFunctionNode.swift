//
//  ASTFunctionNode.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ASTFunctionNode : ASTNode {
    
    static var counter : Int = 0
    
    var name : String
    var parameters : [ASTValueNode] = [ASTValueNode]()
    var closure : ASTClosureNode = ASTClosureNode()
    
    var type : ASTValueType {
        return closure.type
    }
    
    init() {
        
        ASTFunctionNode.counter += 1
        self.name = "func" + ASTFunctionNode.counter.description
        
        
    }
    
    init(name: String) {
        
        self.name = name
        
    }
    
    func addCommand(_ command : ASTCommandNode) {
        
        closure.addCommand(command)
        
    }
    
    func addParameter(_ parameter : ASTValueNode) {
        
        parameters.append(parameter)
        
    }
    
    func accept(_ traversable: ASTTraversable) throws {
        try traversable.createFunction(self)
    }
    
}
