//
//  ASTModule.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ASTModule : ASTNode {
    
    var globals : [ASTVariableNode] = [ASTVariableNode]()
    var functions : [ASTFunctionNode] = [ASTFunctionNode]()
    var mainFunction : ASTFunctionNode = ASTFunctionNode(name: "main")
    
    func addFunction(_ function : ASTFunctionNode) {
        
        functions.append(function)
        
    }
    
    func addGlobal(_ global : ASTVariableNode) {
        
        globals.append(global)
        
    }
    
    func accept(_ traversable: ASTTraversable) throws {
        try traversable.createModule(self)
    }
    
}
