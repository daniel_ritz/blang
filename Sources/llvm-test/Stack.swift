//
//  Stack.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

struct Stack<T> {
    private var items: [T] = []
    
    mutating func push(_ item: T) {
        self.items.append(item)
    }
    
    mutating func pop() -> T {
        return items.removeLast()
    }
    
    func peek() -> T {
        
        guard let item = items.last else { fatalError("empty stack") }
        return item
        
    }
    
    mutating func push(_ item: T, _ f : () throws -> Void) throws {
        
        self.push(item)
        try f()
        _ = self.pop()
        
    }
    
}
