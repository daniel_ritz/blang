

let reader : StreamReader = StdInStreamReader()
let lexer = LexerWorker(reader)

lexer.work()

print(lexer.getTokens())

let parser = ParserWorker(tokens: lexer.getTokens())

try parser.work()

print(parser.getAST())

let compiler = LLVMCompiler(ast: parser.getAST())
try compiler.work()

do{
    try compiler.getModule().print(to: "/Users/daniel/Desktop/out.ll")
}catch{
    
}
