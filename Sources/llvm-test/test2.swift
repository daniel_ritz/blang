import LLVM

func test2(){
let module = Module(name: "main")
let fPrintf = module.addFunction("printf", type: FunctionType(argTypes: [PointerType(pointee: IntType.int8)], returnType:IntType.int32, isVarArg: true))
let fPrint = module.addFunction("print", type: FunctionType([IntType.int64], VoidType()))
let fFib = module.addFunction("fib", type: FunctionType([], VoidType()))
let fMain = module.addFunction("main", type: FunctionType([], VoidType()))




let builder = IRBuilder(module: module)

let i = module.addGlobal("i", initializer: IntType.int64.constant(1))
let j = module.addGlobal("j", initializer: IntType.int64.constant(1))

let fMainEntry = fMain.appendBasicBlock(named: "entry")
builder.positionAtEnd(of: fMainEntry)
builder.buildCall(fFib, args: [])
builder.buildRetVoid()

let fFibEntry = fFib.appendBasicBlock(named: "entry")
let fFibJump = fFib.appendBasicBlock(named: "jump")
let fFibExit = fFib.appendBasicBlock(named: "close")

builder.positionAtEnd(of: fFibEntry)

//let _i = builder.buildLoad(i.constGEP(indices: [IntType.int32.constant(0)]))
let _i = builder.buildLoad(i)
//let _j = builder.buildLoad(j.constGEP(indices: [IntType.int32.constant(0)]))
let _j = builder.buildLoad(j)

let x = builder.buildAdd(_i, _j)

_ = builder.buildStore(_j, to: i)
_ = builder.buildStore(x, to: j)

_ = builder.buildCall(fPrint, args: [x])

let cond = builder.buildICmp(_j, IntType.int64.constant(100), .signedLessThan)
_ = builder.buildCondBr(condition: cond, then: fFibJump, else: fFibExit)

builder.positionAtEnd(of: fFibJump)
builder.buildCall(fMain, args: [])
builder.buildRetVoid()

builder.positionAtEnd(of: fFibExit)
builder.buildRetVoid()

let fPrintEntry = fPrint.appendBasicBlock(named: "entry")
builder.positionAtEnd(of: fPrintEntry)

let format = builder.buildGlobalString("%d", name: "format")

let msg = builder.buildGEP(format, indices: [IntType.int32.constant(0)])
let m = builder.buildBitCast(format, type: PointerType(pointee: IntType.int8))
_ = builder.buildCall(fPrintf, args: [m, fPrint.parameters[0]], name: "ret")
builder.buildRetVoid()



module.dump()

//do{
//    try module.print(to: "/Users/daniel/Desktop/out.ll")
//    try module.emitBitCode(to: "/Users/daniel/Desktop/out.bc")
//}catch ModuleError.couldNotPrint(let e){
//    print(e)
//}


// Set up JIT
//let jit = try JIT(machine: TargetMachine())
//typealias FnPtr = @convention(c) () -> Int
//_ = try jit.addEagerlyCompiledIR(module){ (name) -> JIT.TargetAddress in
//    return JIT.TargetAddress()
//}
//
//let addr = try jit.address(of: "main")
//let fn = unsafeBitCast(addr, to: FnPtr.self)
//print(fn())

}
