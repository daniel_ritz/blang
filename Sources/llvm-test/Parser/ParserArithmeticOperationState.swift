//
//  ParserArithmeticOperationState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserArithmeticOperationState : ParserState {
    
    private let operation : ASTArithmeticNode.Operation
    private let closure : ASTClosureNode
    private let lhs : ASTCommandNode
    
    init(_ context : ParserContext, closure: ASTClosureNode, operation: ASTArithmeticNode.Operation, lhs: ASTCommandNode) {
        
        self.operation = operation
        self.closure = closure
        self.lhs = lhs
        super.init(context)
        
    }
    
    static func forOperation(_ operation : String, context : ParserContext, closure: ASTClosureNode, lhs: ASTCommandNode) throws -> ParserArithmeticOperationState {
        
        return try ParserArithmeticOperationState(context, closure: closure, operation: ASTArithmeticNode.Operation.forSymbol(operation), lhs: lhs)
        
    }
    
    override func handleIdentifier(_ token: LexerToken) throws -> ParserState {
        
        let arithmeticNode = ASTArithmeticNode(lhs: lhs, rhs: context.findGlobal(byName: token.value), operation: operation)
        closure.addCommand(arithmeticNode)
        return context.stack.pop()
        
    }
    
    override func handleNumber(_ token: LexerToken) throws -> ParserState {
        
        let rhs : ASTConstant = try ASTConstant.forValue(token.value)
        
        let arithmeticNode = ASTArithmeticNode(lhs: lhs, rhs: rhs, operation: operation)
        closure.addCommand(arithmeticNode)
        return context.stack.pop()
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value ==  "(" {
            
            // new closure
            let function = context.addFunction()
            let callNode = ASTCallNode(function: function, arguments: [])
            let arithmeticNode = ASTArithmeticNode(lhs: lhs, rhs: callNode, operation: operation)
            closure.addCommand(arithmeticNode)
            context.stack.push(self)
            return ParserClosureState(context, closure: function.closure)
            
        } else {
            throw ParserError.LexicalError(message: "Operation: Bracket " + token.value + " not allowed")
        }
    }
    
}
