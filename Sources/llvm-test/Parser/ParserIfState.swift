//
//  ParserIfState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserIfState : ParserState {
    
    enum InternalState {
        case start
        case condition
        case ifBranch
        case elseBranch
        case end
    }
    
    let closure : ASTClosureNode
    var internalState : InternalState = .start
    var conditionNode : ASTConditionNode?
    
    init(_ context : ParserContext, closure : ASTClosureNode) {
        
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "(" {
            
            if internalState == .start {
                
                internalState = .condition
                return self
                
            } else if internalState == .condition {
                internalState = .ifBranch
                
                context.stack.push(self)
                return ParserComparisonState(context)
                
            } else if internalState == .ifBranch {
                conditionNode = ASTConditionNode(condition: context.comparisonNode!)
                closure.addCommand(conditionNode!)
                
                internalState = .elseBranch
                context.stack.push(self)
                return ParserClosureState(context, closure: conditionNode!.trueClosure)
                
            } else if internalState == .elseBranch {
                internalState = .end
                
                context.stack.push(self)
                return ParserClosureState(context, closure: conditionNode!.falseClosure)
                
            }
            
        } else if token.value == ")" {
            
            if internalState == .elseBranch || internalState == .end {
                
                return context.stack.pop()
                
            }
            
        }
        
        throw ParserError.LexicalError(message: "If: Bracket " + token.value + " not allowed")
        
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        if token.value == "," {
            return self
        } else {
            throw ParserError.LexicalError(message: "If: Token " + token.value + " not allowed")
        }
    }
    
}
