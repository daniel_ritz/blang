//
//  ParserIdentifierState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserIdentifierState : ParserState {
    
    private var closure : ASTClosureNode
    private var identifier : String
    
    init(_ context : ParserContext, closure: ASTClosureNode, identifier: String) {
        
        self.identifier = identifier
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "(" {
            // func call
            let function = context.findFunction(byName: identifier)
            //            context.stack.push(self)
            return ParserFunctionCallState(context, closure: closure, function: function)
        }else{
            throw ParserError.LexicalError(message: "Id: bracket " + token.value + " not allowed")
        }
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        if token.value == ":" {
            // func decl
            let function = context.addFunction(name: identifier)
            closure.addCommand(ASTCallNode(function: function, arguments: []))
            //            context.stack.push(self)
            return ParserFunctionDefState(context, closure: function.closure)
        }else if token.value == "=" {
            //            context.stack.push(self)
            return ParserAssignment(context, closure: closure, identifier: identifier)
        }else if ["+", "-", "/", "*"].contains(token.value) {
            //            context.stack.push(self)
            return try ParserArithmeticOperationState.forOperation(token.value, context: context, closure: closure, lhs: context.findGlobal(byName: identifier))
        }else{
            throw ParserError.LexicalError(message: "Id: Token " + token.value + " not allowed")
        }
    }
    
    override func handleNewline(_ token: LexerToken) throws -> ParserState {
        return self //context.stack.pop()
    }
    
}
