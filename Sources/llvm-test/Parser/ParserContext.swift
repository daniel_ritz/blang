//
//  ParserContext.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ParserContext {
    
    private let module : ASTModule = ASTModule()
    let stack = ParserStack()
    var comparisonNode : ASTComparisonNode?
    
    func addFunction() -> ASTFunctionNode{
    
        let function = ASTFunctionNode()
        module.addFunction(function)
        
        return function
        
    }
    
    func addFunction(name: String) -> ASTFunctionNode {
        
        let function = ASTFunctionNode(name: name)
        module.addFunction(function)
        
        return function
        
    }
    
    func findFunction(byName name: String) -> ASTFunctionNode {
        
        for function in module.functions {
            
            if function.name == name {
                return function
            }
            
        }
        
        return addFunction(name: name)
        
    }
    
    
    func addGlobal() -> ASTVariableNode {
        
        let global = ASTVariableNode()
        module.addGlobal(global)
        
        return global
        
    }
    
    func addGlobal(name: String) -> ASTVariableNode {
        
        let global = ASTVariableNode(name)
        module.addGlobal(global)
        
        return global
        
    }
    
    func findGlobal(byName name: String) -> ASTVariableNode {
        
        for global in module.globals {
            
            if global.name == name {
                return global
            }
            
        }
        
        return addGlobal(name: name)
        
    }
    
    func getAST() -> ASTModule {
        
        return module
        
    }
    
}

class ParserStack {
    
    private var stack = [ParserState]()
    
    func push(_ state : ParserState) {
        
        stack.append(state)
        
    }
    
    func pop() -> ParserState {
        
        return stack.removeLast()
        
    }
    
    func peak() -> ParserState {
        
        return stack.last ?? ParserState(ParserContext())
        
    }
    
}
