//
//  ParserComparisonState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserComparisonState : ParserState {
    
    enum InternalState {
        
        case lhs
        case comparator
        case rhs
        case end
        
    }
    var internalState : InternalState = .lhs
    var comparison : ASTComparisonNode.Comparison?
    var lhs : ASTCommandNode?
    var rhs : ASTCommandNode?
    
    override func handleIdentifier(_ token: LexerToken) throws -> ParserState {
        if internalState == .lhs {
            lhs = context.findGlobal(byName: token.value)
            internalState = .comparator
            return self
        } else if internalState == .rhs {
            rhs = context.findGlobal(byName: token.value)
            internalState = .end
            return self
        } else {
            throw ParserError.LexicalError(message: "Comparison: id " + token.value + " not allowed")
        }
    }
    
    override func handleNumber(_ token: LexerToken) throws -> ParserState {
        if internalState == .lhs {
            lhs = try ASTConstant.forValue(token.value)
            internalState = .comparator
            return self
        } else if internalState == .rhs {
            rhs = try ASTConstant.forValue(token.value)
            internalState = .end
            return self
        } else {
            throw ParserError.LexicalError(message: "Comparison: Number " + token.value + " not allowed")
        }
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "(" {
            if internalState != .lhs && internalState != .rhs {
                throw ParserError.LexicalError(message: "Comparison: Bracket " + token.value + " not allowed")
            }
            let function = context.addFunction()
            let caller = ASTCallNode(function: function, arguments: [])
            if internalState == .lhs {
                lhs = caller
            } else if internalState == .rhs {
                rhs = caller
            }
            
            context.stack.push(self)
            return ParserClosureState(context, closure: function.closure)
            
        } else if token.value == ")" && internalState == .end {
            
            context.comparisonNode = ASTComparisonNode(lhs: lhs!, rhs: rhs!, comparison: comparison!)
            
            return context.stack.pop()
        } else {
            throw ParserError.LexicalError(message: "Comparison: Bracket " + token.value + " not allowed")
        }
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        if ["<", ">", "==", "!=", "<=", ">="].contains(token.value) && internalState == .comparator {
            comparison = try ASTComparisonNode.Comparison.forSymbol(token.value)
            internalState = .rhs
            return self
        } else {
            throw ParserError.LexicalError(message: "Comparison: Token " + token.value + " not allowed")
        }
    }
    
    
}
