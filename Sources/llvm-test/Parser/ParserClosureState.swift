//
//  ParserClosureState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserClosureState : ParserState {
    
    private var closure : ASTClosureNode
    
    init(_ context : ParserContext, closure: ASTClosureNode) {
        
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleIdentifier(_ token: LexerToken) throws -> ParserState {
        // identifier
        context.stack.push(self)
        return ParserIdentifierState(context, closure: closure, identifier: token.value)
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "[" {
            // multi assignment
            context.stack.push(self)
            return ParserMultiAssignment(context, closure: closure)
        } else if token.value == ")" {
            // exit closure
            return context.stack.pop()
        } else {
            throw ParserError.LexicalError(message: "Closure: bracket " + token.value + " not allowed here")
        }
    }
    
    override func handleKeyword(_ token: LexerToken) throws -> ParserState {
        
        if token.value == "if" {
            
            // if assignment
            context.stack.push(self)
            return ParserIfState(context, closure: closure)
            
        } else if token.value == "while" {
            
            context.stack.push(self)
            return ParserLoopState(context, closure: closure)
            
        } else {
            throw ParserError.LexicalError(message: "Closure: Keyword " + token.value + " not allowed")
        }
    }
    
    override func handleNumber(_ token: LexerToken) throws -> ParserState {
        
        context.stack.push(self)
        return ParserNumberState(context, closure: closure, number: try ASTConstant.forValue(token.value))
        
    }
    
}
