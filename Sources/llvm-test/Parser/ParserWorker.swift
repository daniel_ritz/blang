//
//  ParserWorker.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//


class ParserWorker {
    
    let lexerTokens : [LexerToken]
    let context : ParserContext
    var state : ParserState
    
    init(tokens: [LexerToken]) {
        
        self.lexerTokens = tokens
        self.context = ParserContext()
        self.state = ParserModuleState(self.context)
        
    }
    
    func work() throws {
        
        var i = 0
        
        for token in lexerTokens {
            
            do {
                state = try state.handle(token)
            } catch ParserError.LexicalError(let e) {
                
                print(e)
                if i > 1 { print(lexerTokens[i-2].description + lexerTokens[i-1].description) }
                print(token)
                if i < (lexerTokens.count - 2) { print(lexerTokens[i+1].description + lexerTokens[i+2].description) }
                return
                
            }
            
            i += 1
        }
        
    }
    
    func getAST() -> ASTModule {
        
        return context.getAST()
        
    }
    
}

enum ParserError : Error {
    
    case LexicalError(message: String)
    
}
