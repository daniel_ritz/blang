//
//  ParserMultiAssignment.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserMultiAssignment : ParserState {
    
    enum InternalState {
        case variables
        case assignment
        case values
        case end
    }
    
    private var closure : ASTClosureNode
    private var internalState : InternalState = .variables
    
    private var variables = [ASTVariableNode]()
    private var values = [ASTCommandNode]()
    
    init(_ context : ParserContext, closure: ASTClosureNode) {
        
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "]" && internalState == .values {
            internalState = .end
            return self
        }else if token.value == "]" && internalState == .variables {
            internalState = .assignment
            return self
        }else if token.value == "[" && internalState == .values {
            return self
        }else if token.value == "(" && internalState == .values {
            
            let function = context.addFunction()
            let command = ASTCallNode(function: function, arguments: [])
            values.append(command)
            context.stack.push(self)
            return ParserClosureState(context, closure: function.closure)
            
        }else {
            
            throw ParserError.LexicalError(message: "MAssignment: Bracket " + token.value + " not allowed")
            
        }
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        if token.value == "=" && internalState == .assignment {
            internalState = .values
            return self
        } else if token.value == "," && (internalState == .variables || internalState == .values){
            return self
        }else{
            throw ParserError.LexicalError(message: "MAssignment: Token " + token.value + " not allowed")
        }
    }
    
    override func handleIdentifier(_ token: LexerToken) throws -> ParserState {
        if internalState == .variables {
            variables.append(context.findGlobal(byName: token.value))
            return self
        }else if internalState == .values {
            values.append(context.findGlobal(byName: token.value))
            return self
        }else{
            throw ParserError.LexicalError(message: "MAssignment: Id " + token.value + " not allowed")
        }
    }
    
    override func handleNumber(_ token: LexerToken) throws -> ParserState {
        if internalState == .values {
            values.append(try ASTConstant.forValue(token.value))
            return self
        }else{
            throw ParserError.LexicalError(message: "MAssignment: Number " + token.value + " not allowed")
        }
    }
    
    override func handleNewline(_ token: LexerToken) throws -> ParserState {
        if internalState == .end {
            
            closure.addCommand(ASTMultiAssignmentNode(variables: variables, values: values))
            
            return context.stack.pop()
        }else{
            throw ParserError.LexicalError(message: "MAssignment: Newline not allowed")
        }
    }
    
}
