//
//  ParserState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class ParserState {
    
    let context : ParserContext
    
    init(_ context : ParserContext) {
        
        self.context = context
        
    }
    
    func handleBracket(_ token : LexerToken) throws -> ParserState {
        throw ParserError.LexicalError(message: "Bracket " + token.value + " not allowed")
    }
    func handleIdentifier(_ token : LexerToken) throws -> ParserState {
        throw ParserError.LexicalError(message: "Id " + token.value + " not allowed")
        
    }
    func handleNumber(_ token : LexerToken) throws -> ParserState {
        throw ParserError.LexicalError(message: "Number " + token.value + " not allowed")
        
    }
    func handleKeyword(_ token : LexerToken) throws -> ParserState {
        throw ParserError.LexicalError(message: "Keyword " + token.value + " not allowed")
        
    }
    func handleNewline(_ token : LexerToken) throws -> ParserState { return self }
    func handleUnknown(_ token : LexerToken) throws -> ParserState {
        throw ParserError.LexicalError(message: "Token " + token.value + " not allowed")
        
    }
    
    func handle(_ token : LexerToken) throws -> ParserState {
        
        switch token.type {
        case .bracket:
            return try handleBracket(token)
        case .identifier:
            return try handleIdentifier(token)
        case .keyword:
            return try handleKeyword(token)
        case .newline:
            return try handleNewline(token)
        case .number:
            return try handleNumber(token)
        default:
            return try handleUnknown(token)
        }
       
        
    }
    
}

