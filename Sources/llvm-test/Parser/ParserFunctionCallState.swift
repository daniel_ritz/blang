//
//  ParserFunctionCallState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserFunctionCallState : ParserState {
    
    private var closure : ASTClosureNode
    private let function : ASTFunctionNode
    private var assignment : ASTAssignmentNode?
    
    private var arguments = [ASTValueNode]()
    
    init(_ context: ParserContext, closure: ASTClosureNode, function: ASTFunctionNode){
        
        self.closure = closure
        self.function = function
        
        super.init(context)
        
    }
    
    override func handleIdentifier(_ token: LexerToken) throws -> ParserState {
        
        arguments.append(context.findGlobal(byName: token.value))
        return self
        
    }
    
    override func handleNumber(_ token: LexerToken) throws -> ParserState {
        
        arguments.append(try ASTConstant.forValue(token.value))
        return self
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == ")" {
            assignment?.updateType()
            closure.addCommand(ASTCallNode(function: function, arguments: arguments))
            return context.stack.pop()
        }else if token.value == "("{
            // new closure
            let function = context.addFunction()
            let variable = context.addGlobal()
            let command = ASTCallNode(function: function, arguments: [])
            assignment = ASTAssignmentNode(variable: variable, value: command)
            closure.addCommand(assignment!)
            arguments.append(variable)
            context.stack.push(self)
            return ParserClosureState(context, closure: function.closure)
        }else{
            throw ParserError.LexicalError(message: "Function: bracket " + token.value + " not allowed")
        }
    }
    
}
