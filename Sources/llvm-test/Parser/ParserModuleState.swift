//
//  ParserModuleState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserModuleState : ParserState {
    
    override init(_ context: ParserContext) {
        super.init(context)
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "(" {
            
            let function = context.addFunction(name: "main")
            context.stack.push(self)
            return ParserClosureState(self.context, closure: function.closure)
            
        }else {
            
            throw ParserError.LexicalError(message: "Module: bracket not allowed here")
            
        }
    }
    
}
