//
//  ParserLoopState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserLoopState : ParserState {
    
    enum InternalState {
        
        case start
        case condition
        case body
        case end
        
    }
    
    let closure : ASTClosureNode
    var internalState : InternalState = .start
    var loopNode : ASTLoopNode?
    
    init(_ context : ParserContext, closure : ASTClosureNode) {
        
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        
        if token.value == "(" {
            
            if internalState == .start {
                
                internalState = .condition
                return self
                
            } else if internalState == .condition {
                
                internalState = .body
                
                context.stack.push(self)
                return ParserComparisonState(context)
                
            } else if internalState == .body {
                
                loopNode = ASTLoopNode(condition: context.comparisonNode!)
                closure.addCommand(loopNode!)
                
                internalState = .end
                context.stack.push(self)
                return ParserClosureState(context, closure: loopNode!.body)
                
            }
            
        } else if token.value == ")" {
            
            if internalState == .end {
                
                return context.stack.pop()
                
            }
            
        }
        
        throw ParserError.LexicalError(message: "Loop: Bracket " + token.value + " not allowed")
        
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        if token.value == "," {
            return self
        } else {
            throw ParserError.LexicalError(message: "Loop: Token " + token.value + " not allowed")
        }
    }
    
}
