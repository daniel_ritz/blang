//
//  ParserAssignment.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserAssignment : ParserState {
    
    enum InternalState {
        case value
        case end
    }
    
    private var closure : ASTClosureNode
    private var internalState : InternalState = .value
    
    private var variable : ASTVariableNode
    private var value : ASTCommandNode?
    
    init(_ context: ParserContext, closure: ASTClosureNode, identifier: String){
        
        self.closure = closure
        self.variable = context.findGlobal(byName: identifier)
        
        super.init(context)
        
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        throw ParserError.LexicalError(message: "Assignment: Token " + token.value + " not allowed")
    }
    
    override func handleNumber(_ token: LexerToken) throws -> ParserState {
        if internalState == .value {
            value = try ASTConstant.forValue(token.value)
            internalState = .end
            return self
        }else{
            throw ParserError.LexicalError(message: "Assignment: Number " + token.value + " not allowed")
        }
    }
    
    override func handleIdentifier(_ token: LexerToken) throws -> ParserState {
        if internalState == .value {
            value = context.findGlobal(byName: token.value)
            internalState = .end
            return self
        }else{
            throw ParserError.LexicalError(message: "Assignment: Id " + token.value + " not allowed")
        }
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if internalState == .value && token.value == "(" {
            let function = context.addFunction()
            if variable.type != .Int {
                function.closure.type = variable.type
            }
            value = ASTCallNode(function: function, arguments: [])
            context.stack.push(self)
            internalState = .end
            return ParserClosureState(context, closure: function.closure)
        }else{
            throw ParserError.LexicalError(message: "Assignment: Bracket " + token.value + " not allowed")
        }
    }
    
    override func handleNewline(_ token: LexerToken) throws -> ParserState {
        if internalState == .end {
            print(value!.type)
            closure.addCommand(ASTAssignmentNode(variable: variable, value: value!))
            return context.stack.pop()
        }else{
            throw ParserError.LexicalError(message: "Assignment: Newline not allowed")
        }
    }
    
}
