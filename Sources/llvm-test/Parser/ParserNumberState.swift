//
//  ParserNumberState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserNumberState : ParserState {
    
    private var closure : ASTClosureNode
    private var number : ASTConstant
    
    init(_ context : ParserContext, closure : ASTClosureNode, number : ASTConstant) {
        
        self.number = number
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleUnknown(_ token: LexerToken) throws -> ParserState {
        if ["+", "-", "/", "*"].contains(token.value) {
            //            context.stack.push(self)
            return try ParserArithmeticOperationState.forOperation(token.value, context: context, closure: closure, lhs: number)
        }else{
            throw ParserError.LexicalError(message: "Number: Token " + token.value + " not allowed")
        }
    }
    
}
