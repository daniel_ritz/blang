//
//  ParserFunctionDefState.swift
//  llvm-test
//
//  Created by Daniel Ritz on 17.07.19.
//

class ParserFunctionDefState : ParserState {
    
    let closure : ASTClosureNode
    
    init(_ context : ParserContext, closure : ASTClosureNode) {
        
        self.closure = closure
        super.init(context)
        
    }
    
    override func handleBracket(_ token: LexerToken) throws -> ParserState {
        if token.value == "(" {
            context.stack.push(self)
            return ParserClosureState(context, closure: closure)
        } else if token.value == ")" {
            return context.stack.pop()
        } else {
            throw ParserError.LexicalError(message: "FuncDef: Bracket not allowed")
        }
    }
    
}
