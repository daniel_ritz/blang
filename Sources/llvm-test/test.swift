import LLVM

func test() {
print("Hello, world!")


let module = Module(name: "main")

let builder = IRBuilder(module: module)
let putchar = builder.addFunction("putchar", type: FunctionType([IntType.int64], IntType.int32))
let _print = builder.addFunction("print", type: FunctionType([], VoidType()))
let main = builder.addFunction("main", type: FunctionType([], IntType.int64))
let entry = main.appendBasicBlock(named: "entry")
builder.positionAtEnd(of: entry)

let constant = IntType.int64.constant(42)
let sum = builder.buildAdd(constant, constant)

let structure = builder.createStruct(name: "dd", types: [IntType.int64, FloatType.double], isPacked: false)
let s1 = builder.buildAlloca(type: structure)
let ptr0 = builder.buildStructGEP(s1, index: 0)
builder.buildStore(IntType.int64.constant(42), to: ptr0)
let ptr1 = builder.buildStructGEP(s1, index: 1)
builder.buildStore(FloatType.double.constant(33.333), to: ptr1)
let s2 = builder.buildLoad(ptr0)
//let s3 = builder.buildBitCast(s2, type: IntType.int64)

let loc = builder.buildAlloca(type: IntType.int64)
let comp = builder.buildICmp(s2, 0, .equal)

let ifB = main.appendBasicBlock(named: "ifB")
let elseB = main.appendBasicBlock(named: "elseB")
let mergeB = main.appendBasicBlock(named: "mergeB")

builder.buildCondBr(condition: comp, then: ifB, else: elseB)

builder.positionAtEnd(of: ifB)
let ifVal = builder.buildAdd(s2, IntType.int64.constant(2))
builder.buildBr(mergeB)

builder.positionAtEnd(of: elseB)
let elseVal = builder.buildSub(s2, IntType.int64.constant(5))
builder.buildBr(mergeB)

builder.positionAtEnd(of: mergeB)
let phi = builder.buildPhi(IntType.int64)
phi.addIncoming([(ifVal, ifB), (elseVal, elseB)])
builder.buildStore(phi, to: loc)



let ret = builder.buildLoad(loc)
let dbl = builder.buildFPCast(ret, type: FloatType.double)

_ = builder.buildCall(putchar, args: [ret])
_ = builder.buildCall(_print, args: [ret])

builder.buildRet(ret)

module.dump()
//do{
//    try module.print(to: "/Users/daniel/Desktop/out.ll")
//    try module.emitBitCode(to: "/Users/daniel/Desktop/out.bc")
//}catch ModuleError.couldNotPrint(let e){
//    print(e)
//}


// Set up JIT
//let jit = try JIT(machine: TargetMachine())
//typealias FnPtr = @convention(c) () -> Int
//_ = try jit.addEagerlyCompiledIR(module){ (name) -> JIT.TargetAddress in
//    return JIT.TargetAddress()
//}
//
//let addr = try jit.address(of: "main")
//let fn = unsafeBitCast(addr, to: FnPtr.self)
//print(fn())

}
