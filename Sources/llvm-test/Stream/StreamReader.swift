//
//  StreamReader.swift
//  DativeCompiler
//
//  Created by Daniel Ritz on 24.06.19.
//  Copyright © 2019 Daniel Ritz. All rights reserved.
//

import Foundation

protocol StreamReader {
    
    var next: String? { get }
    
}

class StdInStreamReader : StreamReader {
    
    private var currentLine: String?
    
    var next: String? {
        
        if currentLine == nil {
            if readNextLine() == false {
                return nil
            }
        }
        
        if currentLine!.count <= 1 {
            
            let char = currentLine!
            currentLine = nil
            return char
            
        }else{
            
            let index = currentLine!.index(currentLine!.startIndex, offsetBy: 1)
            
            let char: String? = String(currentLine![..<index])
            currentLine = String(currentLine![index...])
            
            return char
        }
        
    }
    
    private func readNextLine() -> Bool{
    
        let line = readLine(strippingNewline: true)
        
        if line == nil {
            return false
        }
        
        currentLine = line! + "\n"
        
        return true
    
    }
    
}
