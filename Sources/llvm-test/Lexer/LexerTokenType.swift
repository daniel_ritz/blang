//
//  LexerToken.swift
//
//  Created by Daniel Ritz on 14.07.19.
//

class LexerToken : CustomStringConvertible {
    
    var value: String
    let type: LexerTokenType
    
    init(_ value: String, type: LexerTokenType) {
        
        self.value = value
        self.type = type
        
    }
    
    var description: String {
    
        return value
        
    }
    
}

enum LexerTokenType {
    
    case newline
    case bracket
    case identifier
    case number
    case keyword
    
    case unknown
    
}
