//
//  LexerWorker.swift
//  llvm-test
//
//  Created by Daniel Ritz on 14.07.19.
//

class LexerWorker {
    
    private var currentToken : LexerToken = LexerToken("", type: .unknown)
    private var token : [LexerToken] = [LexerToken]()
    private var streamReader : StreamReader
    private let keywords : [String] = ["if", "while"]
    
    init(_ streamReader: StreamReader) {
        
        self.streamReader = streamReader
        
    }
    
    public func work() {
        
        while let character : String = streamReader.next {
            
            readCharacter(character)
            
        }
        
    }
    
    private func readCharacter(_ c: String){
        
        if c == "\n" {
            
            handleCurrentToken()
            token.append(LexerToken(c, type: .newline))
            
        } else if ["(", ")", "[", "]"].contains(c) {
            
            handleCurrentToken()
            token.append(LexerToken(c, type: .bracket))
            
        } else if c.matches("\\s") {
            
            handleCurrentToken()
        
        } else if ["+", "-", "/", "*", ",", ":"].contains(c) {
            
            handleCurrentToken()
            token.append(LexerToken(c, type: .unknown))
            
        } else {
            
            currentToken.value += c
            
        }
        
    }
    
    private func handleCurrentToken(){
        
        if currentToken.type == .unknown && currentToken.value == "" { return }
        
        if keywords.contains(currentToken.value) {
            token.append(LexerToken(currentToken.value, type: .keyword))
        }else if currentToken.value.matches("[0-9]+") {
            token.append(LexerToken(currentToken.value, type: .number))
        }else if currentToken.value.matches("[0-9]+\\.[0-9]+") {
            token.append(LexerToken(currentToken.value, type: .number))
        }else if currentToken.value.matches("[\\w\\-_][\\d\\w\\-_]*") {
            token.append(LexerToken(currentToken.value, type: .identifier))
        }else{
            token.append(currentToken)
        }
        
        currentToken = LexerToken("", type: .unknown)
        
    }
    
    public func getTokens() -> [LexerToken] {
        
        return token
        
    }
    
    
}
