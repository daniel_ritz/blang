//
//  LLVMCompilerTraverser.swift
//  llvm-test
//
//  Created by Daniel Ritz on 15.07.19.
//

import LLVM



class LLVMCompiler : ASTTraversable {
    
    let astModule : ASTModule
    let llvmModule : Module
    var functions : [String: Function] = [:]
    var globals : [String: Global] = [:]
    var constants : [String: Constant<Signed>] = [:]
    var builderStack = Stack<IRBuilder>()
    var lastAssignedValue : IRValue?
    
    
    init(ast : ASTModule) {
        self.astModule = ast
        llvmModule = Module(name: "main")
    }
    
    func createModule(_ module: ASTModule) throws {
        
        buildInternalFunctions()
        
        for global in module.globals {
            
            try createGlobalInit(global)
            
        }
        
        for function in module.functions {
            
            try createFunctionStub(function)
            
        }
        
        for function in module.functions {
            
            try function.accept(self)
            
        }
        
    }
    
    func getLLVMType(for type: ASTValueType) -> IRType {
        
        switch type {
        case .Int:
            return IntType.int64
        case .Float:
            return FloatType.double
        case .String:
            return PointerType(pointee: IntType.int8)
        case .Bool:
            return IntType.int1
        }
        
    }
    
    func createFunctionStub(_ function: ASTFunctionNode) throws {
        
        var signature = function.name
        if functions[signature] != nil {
            
            signature += "__"
            for parameter in function.parameters {
                switch parameter.type {
                case .Int: signature += "i"
                case .Float: signature += "f"
                case .String: signature += "s"
                case .Bool: signature += "b"
                }
            }
            
        }
        
        if functions[signature] != nil { return }
        
        let parameterCount = function.parameters.count
        var parameters : [IRType] = [IRType]()
        
        for parameter in function.parameters {
            parameters.append(getLLVMType(for: parameter.type))
        }
        
        let functionType = FunctionType(parameters, getLLVMType(for: function.type))
        let functionDeclaration = llvmModule.addFunction(signature, type: functionType)
        
        functions[signature] = functionDeclaration
        
    }
    
    func findFunction(_ function: ASTFunctionNode) throws -> Function {
        
        var signature = function.name + "__"
        for arg in function.parameters {
            switch arg.type {
            case .Int: signature += "i"
            case .Float: signature += "f"
            case .String: signature += "s"
            case .Bool: signature += "b"
            }
        }
        
        var functionDef : Function? = functions[signature]
        if functionDef == nil {
            // look for function with parameter types
            print("No function " + signature)
            functionDef = functions[function.name]
        }
        guard let functionDeclaration : Function = functionDef! else {
            throw fatalError("function " + function.name + " not found")
        }
        
        return functionDeclaration
        
    }
    
    func findFunction(_ call: ASTCallNode) throws -> Function {
        
        var signature = call.function.name + "__"
        for arg in call.arguments {
            switch arg.type {
            case .Int: signature += "i"
            case .Float: signature += "f"
            case .String: signature += "s"
            case .Bool: signature += "b"
            }
        }
        
        var functionDef : Function? = functions[signature]
        if functionDef == nil {
            // look for function with parameter types
            print("No function " + signature)
            functionDef = functions[call.function.name]
        }
        guard let functionDeclaration : Function = functionDef! else {
            throw fatalError("function " + call.function.name + " not found")
        }
        
        return functionDeclaration
        
    }
    
    func createGlobalInit(_ global: ASTVariableNode) throws {
        
        if globals[global.name] != nil { return }
        
        var globalDefinition : Global? = nil
        switch global.type {
        case .Int:
            globalDefinition = llvmModule.addGlobal(global.name, initializer: IntType.int64.constant(0))
        case .Float:
            globalDefinition = llvmModule.addGlobal(global.name, initializer: FloatType.double.constant(0.0))
        case .Bool:
            globalDefinition = llvmModule.addGlobal(global.name, initializer: IntType.int1.constant(0))
        case .String:
            globalDefinition = llvmModule.addGlobalString(name: global.name, value: "")
        }
        
        globals[global.name] = globalDefinition!
        
    }
    
    func createFunction(_ function: ASTFunctionNode) throws {
        
        let functionDeclaration : Function = try findFunction(function)
    
        
        if function.closure.commands.count == 0 {
            // only declaration
            return
        }
        
        let entryBlock = functionDeclaration.appendBasicBlock(named: "entry")
        
        let builder = IRBuilder(module: llvmModule)
        builder.positionAtEnd(of: entryBlock)
        
        builderStack.push(builder)
//        try builderStack.push(builder) {
        
            try function.closure.accept(self)
            
            let retValue : IRValue = lastAssignedValue ?? IntType.int64.constant(0)
            builder.buildRet(retValue)
//        }
        
        builderStack.pop()
        
    }
    
    func createGlobal(_ global: ASTVariableNode) throws {
        
        let builder = builderStack.peek()
        guard let value = globals[global.name] else {
            throw fatalError("Global " + global.name + " not found")
        }
        
        lastAssignedValue = builder.buildLoad(value)
        
    }
    
    func createIntConstant(_ constant: ASTIntConstant) throws {
        
        let constantDefinition = IntType.int64.constant(constant.value)
        lastAssignedValue = constantDefinition
        
    }
    
    func createFloatConstant(_ constant: ASTFloatConstant) throws {
        
        let constantDefinition = FloatType.double.constant(constant.value)
        lastAssignedValue = constantDefinition
        
    }
    
    func createBoolConstant(_ constant: ASTBoolConstant) throws {
        
        let constantDefinition = IntType.int1.constant(constant.value ? 1 : 0)
        lastAssignedValue = constantDefinition
        
    }
    
    func createStringConstant(_ constant: ASTStringConstant) throws {
        
        let builder = builderStack.peek()
        let constantDefinition = builder.buildGlobalStringPtr(constant.value)
        lastAssignedValue = constantDefinition
        
    }
    
    func createArithmeticOperation(_ op: ASTArithmeticNode) throws {
        
        let builder = builderStack.peek()
        
        try op.lhs.accept(self)
        guard let lhs : IRValue = lastAssignedValue! else { throw fatalError("LHS not found") }
        
        try op.rhs.accept(self)
        guard let rhs : IRValue = lastAssignedValue else { throw fatalError("RHS not found") }
        
        switch op.operation {
        case .add:
            lastAssignedValue = builder.buildAdd(lhs, rhs)
        case .sub:
            lastAssignedValue = builder.buildSub(lhs, rhs)
        case .mul:
            lastAssignedValue = builder.buildMul(lhs, rhs)
        case .div:
            lastAssignedValue = builder.buildDiv(lhs, rhs)
        }
        
    }
    
    func createCall(_ function: ASTCallNode) throws {
        
        let builder = builderStack.peek()

        
        let functionDef : Function = try findFunction(function)
        
        var arguments : [IRValue] = []
        
        for arg in function.arguments {
            
            let value : IRValue = try getValue(arg)
            arguments.append(value)
            
        }
        
        lastAssignedValue = builder.buildCall(functionDef, args: arguments)
        
    }
    
    func createAssignment(_ assignment: ASTAssignmentNode) throws {
        
        let builder = builderStack.peek()
        
        guard let variable : IRValue = globals[assignment.variable.name] else {
            throw fatalError("global " + assignment.variable.name + " not found")
        }
        
        let value : IRValue = try getValue(assignment.value)
        builder.buildStore(value, to: variable)
        
    }
    
    func getValue(_ valueNode : ASTNode) throws -> IRValue {
        
        if let constant = valueNode as? ASTConstant {
            try constant.accept(self)
            guard let value : IRValue = lastAssignedValue else {
                throw fatalError("invalid constant")
            }
            return value
        } else if let global = valueNode as? ASTVariableNode {
            try global.accept(self)
            guard let value : IRValue = lastAssignedValue else {
                throw fatalError("invalid constant")
            }
            return value
        } else if let call = valueNode as? ASTCallNode {
            try call.accept(self)
            guard let value : IRValue = lastAssignedValue else {
                throw fatalError("invalid closure")
            }
            return value
        } else {
            throw fatalError("assignment invalid")
        }
        
    }
    
    func createMultiAssignment(_ assignment: ASTMultiAssignmentNode) throws {
        
        let builder = builderStack.peek()
        var temp : [IRValue] = []
        
        var i = 0
        for singleAssignment in assignment.assignments {
        
            try temp.append(getValue(singleAssignment.value))
            
            i += 1
        }
        
        i = 0
        for singleAssignment in assignment.assignments {
            
            guard let variable : IRValue = globals[singleAssignment.variable.name] else {
                throw fatalError("global " + singleAssignment.variable.name + " not found")
            }
            
            builder.buildStore(temp[i], to: variable)
            
            i += 1
            
        }
        
        
    }
    
    func createComparison(_ comparison: ASTComparisonNode) throws {
        
        let builder = builderStack.peek()
        
        try comparison.lhs.accept(self)
        guard let lhs : IRValue = lastAssignedValue! else { throw fatalError("LHS not found") }
        
        try comparison.rhs.accept(self)
        guard let rhs : IRValue = lastAssignedValue else { throw fatalError("RHS not found") }
        
        switch comparison.comparison {
        case .equals:
            lastAssignedValue = builder.buildICmp(lhs, rhs, .equal)
        case .notEquals:
            lastAssignedValue = builder.buildICmp(lhs, rhs, .notEqual)
        case .lessThan:
            lastAssignedValue = builder.buildICmp(lhs, rhs, .signedLessThan)
        case .lessOrEquals:
            lastAssignedValue = builder.buildICmp(lhs, rhs, .signedLessThanOrEqual)
        case .moreThan:
            lastAssignedValue = builder.buildICmp(lhs, rhs, .signedGreaterThan)
        case .moreOrEquals:
            lastAssignedValue = builder.buildICmp(lhs, rhs, .signedGreaterThanOrEqual)
        }
        
    }
    
    func createCondition(_ condition: ASTConditionNode) throws {
        
        let builder = builderStack.peek()
        
        try condition.condition.accept(self)
        guard let result : IRValue = lastAssignedValue else {
            throw fatalError("invalid condition")
        }
        
        guard let function : Function = builder.currentFunction else {
            throw fatalError("function not found")
        }
        
        let ifBlock = function.appendBasicBlock(named: "if")
        let elseBlock = function.appendBasicBlock(named: "else")
        let mergeBlock = function.appendBasicBlock(named: "merge")
        
        builder.buildCondBr(condition: result, then: ifBlock, else: elseBlock)
        
        let bIf = IRBuilder(module: llvmModule)
        bIf.positionAtEnd(of: ifBlock)
        builderStack.push(bIf)
        try condition.trueClosure.accept(self)
        bIf.buildBr(mergeBlock)
        builderStack.pop()
        
        let bElse = IRBuilder(module: llvmModule)
        bElse.positionAtEnd(of: elseBlock)
        builderStack.push(bElse)
        try condition.falseClosure.accept(self)
        bElse.buildBr(mergeBlock)
        builderStack.pop()
        
        builder.positionAtEnd(of: mergeBlock)
        
    }
    
    func createLoop(_ loop: ASTLoopNode) throws {
        
        let builder = builderStack.peek()
        
        guard let function : Function = builder.currentFunction else {
            throw fatalError("function not found")
        }
        
        let conditionBlock = function.appendBasicBlock(named: "condition")
        let loopBlock = function.appendBasicBlock(named: "loop")
        let doneBlock = function.appendBasicBlock(named: "done")
        
        builder.buildBr(conditionBlock)
        let bCondition = IRBuilder(module: llvmModule)
        bCondition.positionAtEnd(of: conditionBlock)
        builderStack.push(bCondition)
        
        try loop.condition.accept(self)
        guard let result : IRValue = lastAssignedValue else {
            throw fatalError("invalid condition")
        }
        
        bCondition.buildCondBr(condition: result, then: loopBlock, else: doneBlock)
        builderStack.pop()
        
        let bLoop = IRBuilder(module: llvmModule)
        bLoop.positionAtEnd(of: loopBlock)
        builderStack.push(bLoop)
        try loop.body.accept(self)
        bLoop.buildBr(conditionBlock)
        builderStack.pop()
        
        builder.positionAtEnd(of: doneBlock)
        
    }
    
    func createClosure(_ closure: ASTClosureNode) throws {
        
        for command in closure.commands {
            
            try command.accept(self)
            
        }
        
    }
    
    func getModule() -> Module {
        
        return llvmModule
        
    }
    
    func work() throws {
        
        try astModule.accept(self)
        
    }
    
    func buildInternalFunctions(){
        
        let fPrintf = llvmModule.addFunction("printf", type: FunctionType(argTypes: [PointerType(pointee: IntType.int8)], returnType:IntType.int32, isVarArg: true))
        let fPrint = llvmModule.addFunction("print__i", type: FunctionType([IntType.int64], IntType.int64))
        let builder = IRBuilder(module: llvmModule)
        
        let fPrintEntry = fPrint.appendBasicBlock(named: "entry")
        builder.positionAtEnd(of: fPrintEntry)
        
        let format = builder.buildGlobalString("%d", name: "format")
        
        let msg = builder.buildGEP(format, indices: [IntType.int32.constant(0)])
        let m = builder.buildBitCast(format, type: PointerType(pointee: IntType.int8))
        _ = builder.buildCall(fPrintf, args: [m, fPrint.parameters[0]], name: "ret")
        builder.buildRet(IntType.int64.constant(0))
        
        
        
        let fScanf = llvmModule.addFunction("scanf", type: FunctionType([PointerType(pointee: IntType.int8), PointerType(pointee: IntType.int8)], IntType.int32))
        let fScan = llvmModule.addFunction("scan", type: FunctionType([], IntType.int64))
        
        let fScanEntry = fScan.appendBasicBlock(named: "entry")
        builder.positionAtEnd(of: fScanEntry)
        
        let resultInout = builder.addGlobal("scanfResult", initializer: IntType.int64.constant(0))
        let n = builder.buildBitCast(format, type: PointerType(pointee: IntType.int8))
        let r = builder.buildBitCast(resultInout, type: PointerType(pointee: IntType.int8))
        _ = builder.buildCall(fScanf, args: [n, r], name: "ret")
        let ret = builder.buildLoad(resultInout)
        builder.buildRet(ret)
        
        
        let fPrintFloat = llvmModule.addFunction("print__f", type: FunctionType([FloatType.double], IntType.int64))
        let fPrintFloatEntry = fPrintFloat.appendBasicBlock(named: "entry")
        builder.positionAtEnd(of: fPrintFloatEntry)
        
        let formatFloat = builder.buildGlobalString("%f", name: "format.float")
        
        let msgFloat = builder.buildGEP(formatFloat, indices: [IntType.int32.constant(0)])
        let mFloat = builder.buildBitCast(formatFloat, type: PointerType(pointee: IntType.int8))
        _ = builder.buildCall(fPrintf, args: [mFloat, fPrintFloat.parameters[0]], name: "ret")
        builder.buildRet(IntType.int64.constant(0))
        
        
        
        functions["printf"] = fPrintf
        functions["print__i"] = fPrint
        functions["scanf"] = fScanf
        functions["scan"] = fScan
        functions["print__f"] = fPrintFloat
        
    }
    
    
}
