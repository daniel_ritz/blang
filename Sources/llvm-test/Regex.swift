//
//  Regex.swift
//  DativeCompiler
//
//  Created by Daniel Ritz on 01.07.19.
//  Copyright © 2019 Daniel Ritz. All rights reserved.
//

import Foundation

extension String {
    
    public func matches(_ pattern: String) -> Bool {
        
        return self.range(of: "^" + pattern + "$", options: .regularExpression) != nil
        
    }
    
}

