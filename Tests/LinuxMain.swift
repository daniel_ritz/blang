import XCTest

import llvm_testTests

var tests = [XCTestCaseEntry]()
tests += llvm_testTests.allTests()
XCTMain(tests)
